<?php
use yii\widgets\ListView;

echo ListView::widget([
    "dataProvider" => $dataProvider, 
    "itemView" => "_alumnos",
    "options" => [
        "class" => "row"
    ],
    "itemOptions" =>[
        "class" => "col-lg-3 border p-2 m-1",
        "style" => "background-color:#ccc",
    ],
    "summaryOptions" =>[
        "class"=> "col-lg-12",
    ]
]);

