﻿DROP DATABASE IF EXISTS practica007;
CREATE DATABASE practica007;
USE practica007;

CREATE TABLE alumnos(
  codigoAlumno int AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(100),
  telefono varchar(20),
  correo varchar(100),
  PRIMARY KEY(codigoAlumno)
);

